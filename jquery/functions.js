$(document).ready(function(){

	// Change password div

	$("#show_changepass_div").click(function(){
		$("#changepass").show('slow');
	});

	$("#hide_changepass_div").click(function(){
		$("#changepass").hide('slow');
	});

	// Change picture div
	
	$("#show_changepic_div").click(function(){
		$("#changepic").show(600);
	});

	$("#hide_changepic_div").click(function(){
		$("#changepic").hide(600);
	});

	// View top products

	$("#show_tp_div").click(function(){
		$("#top_products").show(600);
	});

	$("#hide_tp_div").click(function(){
		$("#top_products").hide(600);
	});

	// Show and hide reports

	$("#show_reports_div").click(function(){
		$("#reports").show('slow');
	});

	$("#hide_reports_div").click(function(){
		$("#reports").hide('slow');
	});

	// Show products

	$("#show_marvel_products").click(function(){
		$("#marvel_products").show(500);
		$("#dc_products").hide();
	});

	$("#show_dc_products").click(function(){
		$("#dc_products").show(500);
		$("#marvel_products").hide();
	});

	// Buy product
	
	$("#buy_button").click(function(){
		$("#buy_form").show(500);
	});

	// Admin actions

	$("#edit_product_link").click(function(){
		$("#edit_product").show(700);
	});

	$("#delete_product_link").click(function(){
		$("#delete_product").show(700);
	});

	// Image zoom

	$('#zoom_product').elevateZoom({
		zoomType: "window", // window , lens, inner
		cursor: "crosshair",
		zoomWindowFadeIn: 500,
		zoomWindowFadeOut: 750
	});
});
<?php
	error_reporting(0);
	require 'includes/config.inc.php';
	require 'includes/transact.inc.php';

	if(isset($_GET['p'])){
		$p = $_GET['p'];
	}else{
		$p = NULL;
	}

	switch ($p) {
		case 'marvel':
			$page = 'marvel.inc.php';
			$page_title = 'MARVEL Universe';
		break;

		case 'dc':
			$page = 'dc.inc.php';
			$page_title = 'DC Universe';
		break;

		case 'contact':
			$page = 'contactus.inc.php';
			$page_title = 'Contact Us.';
		break;

		case 'register':
			$page = 'register.inc.php';
			$page_title = 'Contact Us.';
		break;
		
		case 'admin':
			$page = 'admin.inc.php';
			$page_title = 'Admin Area';
		break;

		case 'allproducts':
			$page = 'allproducts.inc.php';
			$page_title = 'View Products';
		break;

		case 'viewproduct':
			$page = 'viewproduct.inc.php';
			$page_title = 'View Products';
		break;
		
		default:
			$page = 'main.inc.php';
			$page_title = 'Welcome to Hatchu!';
		break;
	}

	if(!file_exists('modules/' . $page)) {
		$page = 'main.inc.php';
		$page_title = 'Welcome to Hatchu!';
	}

	require 'includes/header.html';
	require 'modules/' . $page;
	require 'includes/footer.html';
?>
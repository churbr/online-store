<?php
	if(!defined('BASE_URL')){
		require '../includes/config.inc.php';
		$url = BASE_URL . '/';
		header('Location: ' . $url);
	}
?>

<div id='contactus_div'>
	
	<h3>Contact Us</h3>

	<hr />



	<img src='images/deadpool.jpg' /> <p>Hi there! My name is Bryan Chu, I created this website just for my final project in PHP. I finished the website in 2 weeks and 3 days, yeah I know I'm slow but hey atleast I finished it. I created this website without using any PHP Framework, and I also created my own web design, no using of bootstrap or foundation framework except for the product zoom, because I have that plugin on my computer already and I used it here to zoom the product image. The front-end and back-end are all pure handcoded from scratch, I mean I really started at zero byte. Now you might be asking why am I not using frameworks. My answer is simple, it is because I enjoy working on it and I don't want it to be finished immediately.</p>
	<p>Some web developers use frameworks to finish their project quickly but that's okay so that they can move on to the next project and earn more money. Handcoding also, is a good practice to master the programming language you want to learn and that's also one of the reasons why I chose to handcode than using modern-tools. I'm not saying that you shouldn't use frameworks, I'm just saying that handcoding is a good start because if you were a handcoder before and now uses modern tools, it's very easy for you to do the project because you're used to do the hard way, handcoding enhances your error debugging skills which is a very important factor to a programmer.</p>
	<p>Some people uses modern development tools to ease and finish their projects quickly but they don't know how to code, one example of these tools is C.M.S where you can just drag and drop, copy and paste, this and that and boom you've just created a website, easy. Which is not good, always remember that these modern tools always have limititions. If you really want to be a good web developer, start from handcoding, don't skip. That's all I can say, if you want to contact me. Here's my contact information:</p>

	<table>
		<tr>
			<td>Yahoo:</td>
			<td>eminence43rd@yahoo.com.ph</td>
		</tr>

		<tr>
			<td>Gmail:</td>
			<td>eminence43rd@gmail.com</td>
		</tr>

		<tr>
			<td>Facebook:</td>
			<td>eminence43rd@facebook.com</td>
		</tr>

		<tr>
			<td>Website:</td>
			<td>eminence43rd.tk</td>
		</tr>
	</table>
</div>
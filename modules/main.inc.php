<header>
	<h2>FEATURED PRODUCT</h2>
</header>

<?php

	if(!defined('BASE_URL')){
		require '../includes/config.inc.php';
		$url = BASE_URL . '/';
		header('Location: ' . $url);
	}

	// $dbc = new mysqli('localhost','root','','storage');
	require DB;

	$query_featured_products = "
		SELECT `products`.`product_id`, `products`.`product_name`, `products`.`product_desc`, `products`.`product_size`, `products`.`product_weight`, `products`.`product_price`, `products`.`product_quantity`, `products`.`product_image`, `products`.`product_category`
		FROM products JOIN details
		ON `products`.`product_id` = `details`.`product_id`
		WHERE `details`.`featured` = 1 AND `products`.`product_quantity` != 0
		ORDER BY `products`.`product_id` DESC
	";

	if($featured_products_stmt = $dbc->prepare($query_featured_products)){
		$featured_products_stmt->execute();

		$featured_products_stmt->bind_result($id, $name, $desc, $size, $weight, $price, $quantity, $image, $category);

		while($featured_products_stmt->fetch()){
?>

<div class='product_box'>
	<h3><?php echo $name;?></h3>
	<img src='images/<?php echo "$category/$image";?>' /> <?php echo substr($desc, 0, 900);?> <a class='see_more_link' href='index.php?p=viewproduct&product=<?php echo $id;?>'>See more...</a>
	</p>

	<table class='product_table'>
		<thead>
			<th colspan='2'>Product Details</th>
		</thead>

		<tr>
			<td class='title'>Name</td>
			<td><?php echo $name;?></td>
		</tr>

		<tr>
			<td class='title'>Price</td>
			<td>P<?php echo number_format($price);?></td>
		</tr>

		<tr>
			<td class='title'>Quantity</td>
			<td><?php echo $quantity;?></td>
		</tr>

		<tr>
			<td class='title'>Size</td>
			<td><?php echo $size;?> " H</td>
		</tr>

		<tr>
			<td class='title'>Weight</td>
			<td><?php echo $weight;?> lbs</td>
		</tr>

		<tr>
			<td colspan='2' style='text-align: center;'>
				<a class='see_more_link' href="index.php?p=viewproduct&product=<?php echo $id;?>">View product</a>
			</td>
		</tr>
	</table>
	<div class='clear'></div>
</div>

<?php
		}
	}
?>
<div class='registration_box'>
	<h3>Registration</h3>

	<table class='registration_table'>
		<form action='' method='post'>
			<tr>
				<td>Username</td>
				<td> <input type='text' name='username' /> </td>
			</tr>

			<tr>
				<td>Password</td>
				<td> <input type='password' name='password' /> </td>
			</tr>

			<tr>
				<td>Repeat-password</td>
				<td> <input type='password' name='repassword' /> </td>
			</tr>

			<tr>
				<td>Firstname</td>
				<td> <input type='text' name='firstname' /> </td>
			</tr>

			<tr>
				<td>Lastname</td>
				<td> <input type='text' name='lastname' /> </td>
			</tr>

			<tr>
				<input type='hidden' name='register' value='true' />
				<td colspan='2' class='register_button'> <input type='submit' value='Register' /> </td>
			</tr>
		</form>
	</table>

	<?php

		if(!defined('BASE_URL')){
			require '../includes/config.inc.php';
			$url = BASE_URL . '/';
			header('Location: ' . $url);
		}

		if(isset($_POST['register'])){
			require 'includes/functions.inc.php';

			$username = trim(htmlentities($_POST['username']));
			$password = trim(htmlentities($_POST['password']));
			$repassword = trim(htmlentities($_POST['repassword']));
			$firstname = trim(htmlentities($_POST['firstname']));
			$lastname = trim(htmlentities($_POST['lastname']));

			echo register_user($username, $password, $repassword, $firstname, $lastname);
		}
	?>

</div>
<?php

	if(!defined('BASE_URL')){
		require '../includes/config.inc.php';
		$url = BASE_URL . '/';
		header('Location: ' . $url);
	}

	require 'includes/functions.inc.php';
	
	if(!isset($_SESSION['username'])){
		header('Location: /');
	}

	$username = $_SESSION['username'];
	$fullname = $_SESSION['firstname'] . ' ' . $_SESSION['lastname'];
	$userimage = $_SESSION['picture'];
?>

<div id='admin_page'>
	
	<img src="<?php echo 'images/' . $userimage; ?>" />

	<fieldset>
		<legend>ADMIN: <?php echo $fullname; ?> </legend>
		<ul>
			<li> <a href='#' id='show_changepass_div'>Change Password</a> </li>
			<li> <a href='#' id='show_changepic_div'>Change Picture</a> </li>
			<li> <a href='#' id='show_tp_div'>Top Products</a> </li>
			<li> <a href='/allproducts'>View Products</a> </li>
			<li> <a href='#' id='show_reports_div'>Show Reports</a> </li>
			<li> <a href='logout.php'>Logout</a> </li>
		</ul>
	</fieldset>

	<div class='clear'></div>

	<div id='top_products'>
		<h2>Top Products</h2>

		<table>
			<thead>
				<th>Product Name</th>
				<th>Sold Quantity</th>
			</thead>

			<?php
				display_top_products();
			?>
			
		</table>

		<hr />
		<a href='#' id='hide_tp_div'>Close</a>
		<div class='clear'></div>
	</div>

	<div id='reports'>
		<h2>Product Reports</h2>

		<hr />
		
		<table>
			<?php display_sales_reports(); ?>
		</table>

		<?php display_income(); ?>

		<hr />
		<a href='#' id='hide_reports_div'>Close</a>
		<div class='clear'></div>
	</div>

	<div id='changepic'>
		<h3>Change Picture</h3>
		<hr />
		<form action='' enctype='multipart/form-data' method='post'>
			<p> <input type='file' name='userimage_upload' /> </p>
			<input type='hidden' name='changedpic' value='true' />
			<p> <input class='changepic_button' type='submit' value='Change picture' /> </p>
		</form>

		<a href='#' id='hide_changepic_div'>Close</a>
		<div class='clear'></div>
	</div>


	<div id='changepass'>
		<h3>Change password</h3>
		<hr />
		<table>
			<form action='' method='post'>
				<tr>
					<td>Old password</td>
					<td> <input type='password' name='oldpassword' /> </td>
				</tr>

				<tr>
					<td>New Password</td>
					<td> <input type='password' name='password' /> </td>
				</tr>

				<tr>
					<td>Repeat-password</td>
					<td> <input type='password' name='repassword' /> </td>
				</tr>

				<tr>
					<input type='hidden' name='changedpass' value='true' />
					<td colspan='2' style="text-align: center;"> <input class='changepass_button' type='submit' value='Change password'> </td>
				</tr>
			</form>
		</table>

		<a href='#' id='hide_changepass_div'>Close</a>
		<div class='clear'></div>
	</div>

	<?php
		if(isset($_POST['changedpass'])){

			$oldpass = trim(htmlentities($_POST['oldpassword']));
			$newpass = trim(htmlentities($_POST['password']));
			$repass = trim(htmlentities($_POST['repassword']));

			echo change_password($oldpass, $newpass, $repass, $_SESSION['username']);
		}

		elseif(isset($_POST['changedpic'])){
			echo change_picture($_FILES['userimage_upload']);
		}
	?>

	<hr />
	
	<div id='addproduct'>
		<h3>Add New Product</h3>

		<?php
			if(isset($_POST['new_product_added'])) {
				$product_info_submitted = TRUE;
				$problem = FALSE;
			}
		?>

		<form action='' enctype='multipart/form-data' method='post'>

			<table id='addproducts_table'>
				<tr>
					<td>Upload Image</td>
					<td> <input type='file' name='product_image' />
						<?php
							if($product_info_submitted){
								if(check_product_image($_FILES['product_image'])==TRUE){
									$product_image = $_FILES['product_image']['tmp_name'];
									$product_image_name = $_FILES['product_image']['name'];
								}else{
									$problem = TRUE;
									echo "<p id='error'>Something is wrong with the image.</p>";
								}
							}
						?>
					</td>
				</tr>

				<tr>
					<td>Product Name</td>
					<td> <input type='text' name='product_name' value="<?php echo isset($_POST['product_name']) ? filter($_POST['product_name']):'';?>" />
						<?php
							if($product_info_submitted) {
								$product_name = $_POST['product_name'];
								check_product_name($product_name);
							}
						?>
					</td>
				</tr>

				<tr>
					<td>Description</td>
					<td>
						<textarea name='product_desc' rows='5' cols='35'><?php
							echo isset($_POST['product_desc']) ? filter($_POST['product_desc']):'';
						?></textarea>
						<?php
							if($product_info_submitted){
								$product_desc = $_POST['product_desc'];
								check_product_desc($product_desc);
							}
						?>
					</td>
				</tr>

				<tr>
					<td>Price</td>
					<td> <input type='text' name='product_price' size='10' value="<?php echo isset($_POST['product_price']) ? filter($_POST['product_price']) : ''; ?>" /> Php
						<?php
							if($product_info_submitted){
								$product_price = $_POST['product_price'];
								check_product_price($product_price);
							}
						?>
					</td>
				</tr>

				<tr>
					<td>Quantity</td>
					<td> <input type='text' name='product_quantity' size='2' value="<?php echo isset($_POST['product_quantity']) ? intval(filter($_POST['product_quantity'])) : ''; ?>" /> Piece(s)
						<?php
							if($product_info_submitted){
								$product_quantity = $_POST['product_quantity'];
								check_product_quantity($product_quantity);
							}
						?>
					</td>
				</tr>

				<tr>
					<td>Category</td>
					<td>
						<input type='radio' name='product_category' value='marvel' checked/> MARVEL <br />
						<input type='radio' name='product_category' value='dc' /> DC
						<?php
							if($product_info_submitted){
								$product_category = $_POST['product_category'];
								check_product_category($product_category);
							}
						?>
					</td>
				</tr>

				<tr>
					<td>Size</td>
					<td> <input type='text' name='product_size' size='5' value="<?php echo isset($_POST['product_size']) ? number_format($_POST['product_size'], 2) : ''; ?>" /> " H
						<?php
							if($product_info_submitted){
								$product_size = $_POST['product_size'];
								check_product_size($product_size);
							}
						?>
					</td>
				</tr>

				<tr>
					<td>Weight</td>
					<td> <input type='text' name='product_weight' size='5' value="<?php echo isset($_POST['product_weight']) ? number_format($_POST['product_weight'], 2) : ''; ?>" /> lbs
						<?php
							if($product_info_submitted){
								$product_weight = $_POST['product_weight'];
								check_product_weight($product_weight);
							}
						?>
					</td>
				</tr>

				<tr>
					<td>License</td>
					<td> <input type='text' name='product_license' value="<?php echo isset($_POST['product_license']) ? filter($_POST['product_license']) : ''; ?>" />
						<?php
							if($product_info_submitted){
								$product_license = $_POST['product_license'];
								validate_string($product_license);
							}
						?>
					</td>
				</tr>

				<tr>
					<td>Manufacturer</td>
					<td> <input type='text' name='product_manufacturer' value="<?php echo isset($_POST['product_manufacturer']) ? filter($_POST['product_manufacturer']) : ''; ?>" />
						<?php
							if($product_info_submitted){
								$product_manufacturer = $_POST['product_manufacturer'];
								validate_string($product_manufacturer);
							}
						?>
					</td>
				</tr>

				<tr>
					<td>Feature this?</td>
					<td>
						<input type='radio' name='feature' value='yes' />Yes <br />
						<input type='radio' name='feature' value='no' checked/> No
						<?php
							if($product_info_submitted){
								$selected_option = $_POST['feature'];
								check_selected_option($selected_option);
							}
						?>
					</td>
				</tr>

				<tr>
					<input type='hidden' name='new_product_added' value='true' />
					<td colspan='2' style="text-align: center;"> <input class='addproduct_button' type='submit' value='Add New Product!' /> </td>
				</tr>

				<?php
					if($product_info_submitted){
						if(!$problem){
							$product_image_name = make_random_name($product_image_name);
							move_product_image($product_image, $product_image_name, $product_category);
							INSERT_TO_DB($product_image_name, $product_name, $product_desc, $product_price, $product_quantity, $product_category, $product_size, $product_weight, $product_license, $product_manufacturer, $selected_option);
						}
					}
				?>
			</table>
		</form>
	</div>
</div>
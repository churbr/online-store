<div id='view_product'>

<?php

	if(!defined('BASE_URL')){
		require '../includes/config.inc.php';
		$url = BASE_URL . '/';
		header('Location: ' . $url);
	}

	if(!is_array(view_product_data($_GET['product']))){
		echo "<p id='error'>Product is not available.</p>";
	}else{
		static $static_product_id;
		$product = array(view_product_data($_GET['product']));

		$static_product_id = $product[0]['product_id']; // for security

		// If the customer press buy product

		if(isset($_POST['bought'])){
			$ccpin = $_POST['ccpin'];
			$quantity = $_POST['quantity'];

			PROCESS_TRANSACTION($ccpin, $quantity, $static_product_id);
		}

		/* If delete is confirmed */
		if(isset($_POST['delete_confirmed'])) {
			if($static_product_id===intval($_POST['delete_product_id'])) {
				DELETE_PRODUCT($static_product_id, $_POST['confirm_delete']);
			}
		}

		/* If product is edited */
		if(isset($_POST['product_edited'])) {

			$product_id 			= $_POST['product_id'];
			$product_name 			= $_POST['product_name'];
			$product_price 			= $_POST['product_price'];
			$product_quantity 		= $_POST['product_quantity'];
			$product_category 		= $_POST['product_category'];
			$product_size 			= $_POST['product_size'];
			$product_weight 		= $_POST['product_weight'];
			$product_license 		= $_POST['product_license'];
			$product_manufacturer 	= $_POST['product_manufacturer'];
			$feature 				= $_POST['feature'];
			$sold 					= $_POST['sold'];

			$updated_data = check_edited_product($static_product_id, $product_id, $product_name, $product_price, $product_quantity, $product_category, $product_size, $product_weight, $product_license, $product_manufacturer, $feature, $sold);

			if(is_array($updated_data)) {
				$product_reference_id	= $updated_data['id'];
				$updated_name 			= $updated_data['edited_name'];
				$updated_price 			= $updated_data['edited_price'];
				$updated_quantity 		= $updated_data['edited_quantity'];
				$updated_category 		= $updated_data['edited_category'];
				$updated_size 			= $updated_data['edited_size'];
				$updated_weight 		= $updated_data['edited_weight'];
				$updated_license 		= $updated_data['edited_license'];
				$updated_manufacturer 	= $updated_data['edited_manufacturer'];
				$updated_feature 		= $updated_data['edited_feature'];
				$updated_sold			= $updated_data['edited_sold'];

				UPDATE_PRODUCTS($product_reference_id, $updated_name, $updated_price, $updated_quantity, $updated_category, $updated_size, $updated_weight, $updated_license, $updated_manufacturer, $updated_feature, $updated_sold);
			}
		}

		$product_image_loc = 'images/' . strtolower($product[0]['product_category']) . '/' . strtolower($product[0]['product_image']);
?>

	<ul>
		<li> <a href='/allproducts'> ALL PRODUCTS </a> </li>
		<li> <a href='/'> FEATURED PRODUCTS </a> </li>
	</ul>

	<div class='clear'></div>

	<h2> <?php echo $product[0]['product_name']; ?> </h2>

	<div id='buy_form'>
		<h1>Confirm Purchase</h1>
		<table>
			<form action='' method='POST'>
				<tr>
					<td>Credit Card Pin</td>
					<td> <input type='text' name='ccpin' /> </td>
				</tr>

				<tr>
					<td>Quantity</td>
					<td> <input type='text' name='quantity' size='3' /> </td>
				</tr>

				<tr>
					<input type='hidden' name='bought' value='true' />
					<td colspan='2' style='text-align: center;'> <input class='buy_button' type='submit' value='CONFIRM' /> </td>
				</tr>
			</form>
		</table>
	</div>

<?php
	if(is_loggedin()){
?>

	<div id='edit_product'>
		<h1>Edit Product Information</h1>

		<hr />

		<table>
			<form action='' method='POST'>
				<tr>
					<td class='title'>Name</td>
					<td> <input type='text' name='product_name' value="<?php echo $product[0]['product_name']; ?>" /> </td>
				</tr>

				<tr>
					<td class='title'>Price</td>
					<td> <input type='text' name='product_price' size='7' value="<?php echo $product[0]['product_price']; ?>" /> Php </td>
				</tr>


				<tr>
					<td class='title'>Quantity</td>
					<td> <input type='text' name='product_quantity' size='3' value="<?php echo $product[0]['product_quantity']; ?>" /> pieces </td>
				</tr>

				<tr>
					<td class='title'>Category</td>
					<td> <input type='text' name='product_category' value="<?php echo $product[0]['product_category']; ?>" /> </td>
				</tr>

				<tr>
					<td class='title'>Size</td>
					<td> <input type='text' name='product_size' size='5' value="<?php echo $product[0]['product_size']; ?>" /> " H </td>
				</tr>

				<tr>
					<td class='title'>Weight</td>
					<td> <input type='text' name='product_weight' size='3' value="<?php echo $product[0]['product_weight']; ?>" /> lbs </td>
				</tr>

				<tr>
					<td class='title'>License</td>
					<td> <input type='text' name='product_license' value="<?php echo $product[0]['product_license']; ?>" /> </td>
				</tr>

				<tr>
					<td class='title'>Manufacturer</td>
					<td> <input type='text' name='product_manufacturer' value="<?php echo $product[0]['product_manufacturer']; ?>" /> </td>
				</tr>

				<tr>
					<td class='title' style='vertical-align: top;'>Feature this?</td>
					<td>
						<input type='radio' name='feature' value='yes'> yes <br />
						<input type='radio' name='feature' value='no' checked /> no
					</td>
				</tr>

				<tr>
					<td class='title' style='vertical-align: top;'>Sold?</td>
					<td>
						<input type='radio' name='sold' value='yes'> yes <br />
						<input type='radio' name='sold' value='no' checked /> no
					</td>
				</tr>

				<tr>
					<td colspan='2' style='text-align: center;'>
						<input type='hidden' name='product_edited' value='true' />
						<input type='hidden' name='product_id' value='<?php echo $product[0]['product_id'];?>' />
						<input class='edit_product_button' type='submit' value='Edit Product' />
					</td>
				</tr>
			</form>
		</table>
	</div>

	<div id='delete_product'>
		<form action='' method='POST'>
			<h1>Please confirm to delete the product.</h1> <hr />
			<p> <input type='radio' name='confirm_delete' value='yes' checked /> YES </p>
			<p> <input type='radio' name='confirm_delete' value='no' /> NO </p>
			<input type='hidden' name='delete_product_id' value='<?php echo $product[0]['product_id'];?>' />
			<input type='hidden' name='delete_confirmed' value='true' />
			<input class='delete_product_botton'type='submit' value='OK' />
		</form>
	</div>
<?php
	}
?>
	<img id='zoom_product' src="<?php echo $product_image_loc; ?>" data-zoom-image="<?php echo $product_image_loc; ?>" />
	
	<table id='product_info_view'>
		<thead>
			<tr>
				<th colspan='2'>Product Info</th>
			</tr>
		<?php if(is_loggedin()){ ?>
			<tr id='admin_action'>
				<th> <a id='edit_product_link' href='#'>[Edit Product]</a> </th>
				<th> <a id='delete_product_link' href='#'>[Delete Product]</a> </th>
			</tr>
		<?php } ?>
		</thead>

		<tr>
			<td class='title'>Product name</td>
			<td> <?php echo $product[0]['product_name']; ?> </td>
		</tr>

		<tr>
			<td class='title'>Price</td>
			<td> P<?php echo number_format($product[0]['product_price'], 2); ?> </td>
		</tr>

		<tr>
			<td class='title'>Quantity</td>
			
			<td>
				<?php
					$product_remaining = $product[0]['product_quantity'];
					echo ($product_remaining!=0) ?  $product_remaining . ' pieces': "<p id='error'>NOT AVAILABLE</p>";
				?>
			</td>
		</tr>
		
		<tr>
			<td class='title'>Category</td>
			<td> <?php echo strtoupper($product[0]['product_category']); ?> </td>
		</tr>

		<tr>
			<td class='title'>Size</td>
			<td> <?php echo $product[0]['product_size']; ?>  " H</td>
		</tr>

		<tr>
			<td class='title'>Weight</td>
			<td> <?php echo $product[0]['product_weight']; ?> lbs</td>
		</tr>

		<tr>
			<td class='title'>License</td>
			<td> <?php echo $product[0]['product_license']; ?> </td>
		</tr>

		<tr>
			<td class='title'>Manufacturer</td>
			<td> <?php echo $product[0]['product_manufacturer']; ?> </td>
		</tr>

		<tr>
			<td class='title'>Date Added</td>
			<td> <?php echo $product[0]['date_added']; ?> </td>
		</tr>

		<tr>
			<td class='title'>Seller</td>
			<td> <?php echo $product[0]['author']; ?> </td>
		</tr>

		<tr>
			<td colspan='2'>
				<a id='buy_button' href='#'>BUY PRODUCT<img src='images/cart.png' /> </a>
			</td>
		</tr>
	</table>

	<div class='clear'></div>

	</div>
<?php
	}
?>
</div>
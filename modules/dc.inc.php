<div id='category_image_div'>
	<div class='category_logo'>
		<img src='images/dc.jpg' />
	</div>
</div>

<div class='clear'></div>

<div id='product_category_view'>

<?php

	if(!defined('BASE_URL')) {
		require '../includes/config.inc.php';
		$url = BASE_URL . '/';
		header('Location: ' . $url);
	}

	require 'includes/transaction_queries.inc.php';
	require DB;

	if($view_dc_category_stmt = $dbc->prepare($view_dc_category_query)){
		$view_dc_category_stmt->execute();

		$view_dc_category_stmt->bind_result($id, $name, $image, $quantity, $price);
		
		while($view_dc_category_stmt->fetch()){
?>

		<div class='each_category_product'>
			<h4><?php echo $name;?></h4>

			<img src='images/dc/<?php echo $image;?>' />

			<h6>(<?php echo $quantity;?> available)</h6>
			
			<p class='price'>Php<?php echo number_format($price);?></p>

			<hr />

			<a href='index.php?p=viewproduct&product=<?php echo $id;?>'>
				Buy now <img src='images/buy.png' />
			</a>
		</div>

<?php
		}

		$view_dc_category_stmt->free_result();
		$dbc->close();
	}
?>

	<div class='clear'></div>
</div>
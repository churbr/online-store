<?php

	if(!defined('BASE_URL')){
		require '../includes/config.inc.php';
		$url = BASE_URL . '/';
		header('Location: ' . $url);
	}

	require 'includes/transaction_queries.inc.php';
	require DB;
?>

<div id='view_products'>
	<nav>
		<a href='#' id='show_marvel_products'>MARVEL</a>
		<a href='#' id='show_dc_products'>DC</a>
	</nav>

	<div id='products_div'>
	
		<div id='marvel_products'>
			<?php
				if($fetch_marvel_prod = $dbc->query($fetch_all_marvel_query)) {
					if($fetch_marvel_prod->num_rows) {
						while($data = $fetch_marvel_prod->fetch_object()) {
							$id = $data->product_id;
							$name = $data->product_name;
							$image = $data->product_image;

							echo <<<MARVEL
								<div class='each_product'>
									<h3>$name</h3>
									<img src='images/marvel/$image' />
									<p> <a href='index.php?p=viewproduct&product=$id'>View Product</a> </p>
								</div>
MARVEL;
						}
					}else{
						echo "<p id='error'>No products available.</p>";
					}
				}
			?>
		</div>

		<div id='dc_products'>
			<?php
				if($fetch_dc_prod = $dbc->query($fetch_all_dc_query)) {
					if($fetch_dc_prod->num_rows) {
						while($data = $fetch_dc_prod->fetch_object()) {
							$id = $data->product_id;
							$name = $data->product_name;
							$image = $data->product_image;

							echo <<<DC
								<div class='each_product'>
									<h3>$name</h3>
									<img src='images/dc/$image' />
									<p> <a href='index.php?p=viewproduct&product=$id'>View Product</a> </p>
								</div>
DC;
						}
					}else{
						echo "<p id='error'>No products available.</p>";
					}
				}
			?>
		</div>

	</div>
</div>
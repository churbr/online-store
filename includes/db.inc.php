<?php
	error_reporting(0);

	$config['DB_HOST'] = 'localhost';
	$config['DB_USERNAME'] = 'root';
	$config['DB_PASSWORD'] = '';
	$config['DB_NAME'] = 'storage';

	foreach ($config as $key => $value) {
		define(strtoupper($key), $value);
	}

	$dbc = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

	if($dbc->connect_errno){
		die( $dbc->connect_error );
	}
?>
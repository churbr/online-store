<?php
	error_reporting(0);

	function filter($string){
		return strip_tags(trim($string));
	}

	function check_username($username){
		return preg_match('#(?!^\d+$)^.+$#', $username);
	}

	function check_name($firstname, $lastname) {
		if( (preg_match('#^[a-zA-Z\s]*$#', $firstname)==1) && (preg_match('#^[a-zA-Z\s]*$#', $lastname)==1) ){
			return true;
		}else{
			return false;
		}
	}

	function register_user($username, $password, $repassword, $firstname, $lastname){
		require DB;
		$message = '';

		if(!empty($username) && !empty($password) && !empty($firstname) && !empty($lastname) ){
			
			$existed = $dbc->query("SELECT `username` FROM `admin` WHERE `username` = '$username'");

			if(!$existed->num_rows){
				if(strlen($password)>=10){
					if(strcmp($password, $repassword)===0){
						if((strlen($firstname)>1 && strlen($firstname)<=20 && strlen($lastname)>1 && strlen($lastname)<=20)){
							if(check_name($firstname, $lastname)==true){
								if(check_username($username)==1){
									if(strlen($username)>=7 && strlen($username)<30){

										#  @@@@@@@@@@@@ DATABASE PROCESS @@@@@@@@@@@@ #

										$password = md5($password);

										$statement1 = $dbc->prepare("INSERT INTO `admin` (`user_id`, `username`, `password`) VALUES(NULL, ?, ?)");
										$statement2 = $dbc->prepare("INSERT INTO `profile` (`profile_id`, `firstname`, `lastname`, `picture`, `status`, `user_id`) VALUES (NULL, ?, ?, 'default.png', 1, ( SELECT `user_id` FROM `admin` WHERE `username` = ? ) )");

										$statement1->bind_param('ss', $username, $password);
										$statement1->execute();
										
										$statement2->bind_param('sss', $firstname, $lastname, $username);
										$statement2->execute();

										$admin = $statement1->affected_rows;
										$profile = $statement2->affected_rows;

										if($admin==1 && $profile==1){
											$message .= "<p id='success'>You have successfully registered.</p>";
										}else{
											$message .= "<p id='error'>User could not be registered.</p>";
										}

										#  @@@@@@@@@@@@ DATABASE PROCESS @@@@@@@@@@@@ #

									}else{
										$message .= "<p id='error'>Length of the username must be atleast 7 - 29 characters.</p>";
									}
								}else{
									$message .= "<p id='error'>Username must not be only numeric.</p>";
								}
							}else{
								$message .= "<p id='error'>Your name must contain only letters.</p>";
							}
						}else{
							$message .= "<p id='error'>Your firstname/lastname character length is too short or too long.</p>";
						}
					}else{
						$message .= "<p id='error'>Passwords do not matched.</p>";
					}
				}else{
					$message .= "<p id='error'>Password must be atleast 10 characters long.</p>";
				}
			}else{
				$message .= "<p id='error'>Username already taken.</p>";
			}
		}else{
			$message .= "<p id='error'>Please complete the form.</p>";
		}

		return $message;
	}


	function auth_credentials($username, $password){
		require DB;
		$login_message = '';

		if(!empty($username) && !empty($password)){
			if($login_statement = $dbc->prepare("SELECT `admin`.`user_id`, `admin`.`username`, `admin`.`password`, `profile`.`firstname`, `profile`.`lastname`, `profile`.`picture`, `profile`.`status` FROM `admin` JOIN `profile` ON `admin`.`user_id` = `profile`.`profile_id` WHERE `username` = ?")){
				$login_statement->bind_param('s', $username);
				$login_statement->execute();
			}

			$login_statement->store_result();

			if($login_statement->num_rows){

				$login_statement->bind_result($dbuserid, $dbusername, $dbpassword, $dbfirstname, $dblastname, $dbpicture, $dbstatus);
				$login_statement->fetch();

				if(strcmp(md5($password), $dbpassword)==0){
					if($dbstatus===1){
						session_start();
						$_SESSION['user_id'] = $dbuserid;
						$_SESSION['username'] = $dbusername;
						$_SESSION['firstname'] = $dbfirstname;
						$_SESSION['lastname'] = $dblastname;
						$_SESSION['picture'] = $dbpicture;

						header('Location: /admin');
					}else{
						$login_message .= "<p id='error'>Please activate your account first.</p>";
					}
				}else{
					$login_message .= "<p id='error'>Incorrect password.</p>";
				}

			}else{
				$login_message .= "<p id='error'>Account does not exist.</a>";
			}
		}else{
			$login_message .= "<p id='error'>Please input your username && password.</p>";
		}

		return $login_message;
	}

	function change_password($oldpass, $newpass, $repass, $username){
		require DB;
		$changepass_message = '';

		if(!empty($oldpass) && !empty($newpass) && !empty($repass)){
			
			if($changepass_statement = $dbc->prepare("SELECT `admin`.`password` FROM `admin` WHERE `username` = ?")){
				$changepass_statement->bind_param('s', $username);
				$changepass_statement->execute();
			}

			$changepass_statement->bind_result($dboldpass);
			$changepass_statement->fetch();
			$changepass_statement->close();

			if(strcmp(md5($oldpass), $dboldpass)===0){
				if(strcmp($newpass, $repass)===0){
					if(strlen($newpass)>=10){
						/* ################## DB PROCESS ################## */

						if($changepass_statement = $dbc->prepare("UPDATE `admin` SET `password` = ? WHERE `username` = ?")){
							$changepass_statement->bind_param('ss', md5($newpass), $username);
							$changepass_statement->execute();
						}else{
							$changepass_message .= "<p id='error'>Could not change the password.</p>";
						}

						if($dbc->affected_rows){
							$changepass_message .= "<p id='error'>Password successfully updated.</p>";
						}

						/* ############### END OF DB PROCESS ############### */
					}else{
						$changepass_message .= "<p id='error'>Password is not secured.</p>";
					}
				}else{
					$changepass_message .= "<p id='error'>Passwords didn't match.</p>";
				}
			}else{
				$changepass_message .= "<p id='error'>Incorrect old password.</p>";
			}

		}else{
			$changepass_message .= "<p id='error'>Please complete the form.</p>";
		}

		return $changepass_message;
	}

	function make_random_name($file_name) {
		// Generate a random filename
		$characters = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,,q,r,s,t,u,v,w,x,y,z,1,2,3,4,5,6,7,8,,9,0';
		$characters_array = explode(',', $characters);
		shuffle($characters_array);
		$randomed_filename = implode('', $characters_array);

		// Get file extension
		$file_extension = end(explode('.', $file_name));

		// New filename
		return ($randomed_filename.'.'.$file_extension);
	}

	function change_picture(&$userimage_upload){
		require DB;
		$changepic_message = '';

		$file = $userimage_upload['tmp_name'];
		$file_type = $userimage_upload['type'];
		$file_name = $userimage_upload['name'];

		if(!empty($file) && !empty($file_type) && !empty($file_name)){
			if(strcmp('image', substr($file_type, 0, 5))===0){

				// New filename
				$new_filename = make_random_name($file_name);

				if(getimagesize($file)==TRUE){
					if($changepic_statement = $dbc->prepare("UPDATE `profile` SET `picture` = ? WHERE `profile_id` = ?")){
						$changepic_statement->bind_param('si', $new_filename, $_SESSION['user_id']);
						$changepic_statement->execute();

						if((move_uploaded_file($file, 'images/'.$new_filename)) && ($dbc->affected_rows===1)){
							$_SESSION['picture'] = $new_filename;
							header('Location: /admin');
							exit();
						}else{
							$changepic_message .= "<p id='error'>Could not change picture.</p>";
						}
					}
				}else{
					$changepic_message .= "<p id='error'>Not an image.</p>";
				}
			}else{
				$changepic_message .= "<p id='error'>Only images are allowed.</p>";
			}
		}else{
			$changepic_message .= "<p id='error'>Please select an image.</p>";
		}

		return $changepic_message;
	}

	/* @@@@@@@@@@@@@@@@@@@@@@ ADD PRODUCT FUNCTIONS @@@@@@@@@@@@@@@@@@@@@@ */

	function check_product_image(&$product_image){
		$file 		= $product_image['tmp_name'];
		$file_type	= $product_image['type'];
		$file_name	= $product_image['name'];
		$file_error = FALSE;

		if(!empty($file) && !empty($file_type) && !empty($file_name)){

			$check_product_image_msg = '';

			if(strcmp('image', substr($file_type, 0, 5))===0){
				if(getimagesize($file)==TRUE){
					return TRUE;
				}
			}
		}

		return $file_error;
	}

	function check_product_name(&$product_name){
		global $problem;

		if(!empty($product_name)){
			if( (strlen($product_name)>=6) && (strlen($product_name)<=100)){
				$product_name = filter($product_name);
			}else{
				$problem = TRUE;
				echo "<p id='error'>Fix your product name character length.</p>";
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>Please input your product name.</p>";
		}
	}

	function check_product_desc(&$product_desc){
		global $problem;

		if(!empty($product_desc)){
			if(strlen($product_desc)>=12){
				$product_desc = filter($product_desc);
			}else{
				$problem = TRUE;
				echo "<p id='error'>Description should be atleast 12 characters long.</p>";
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>Please input something on product description.</p>";
		}
	}

	function check_product_price(&$product_price) {
		global $problem;

		if(is_numeric($product_price)){
			$product_price = floatval($product_price);
		}else{
			$problem = TRUE;
			echo "<p id='error'>This is not a number.</p>";
		}
	}

	function check_product_quantity(&$product_quantity) {
		global $problem;

		if(is_numeric($product_quantity)){
			$product_quantity = intval($product_quantity);
		}else{
			$problem = TRUE;
			echo "<p id='error'>Invalid product quantity input.</p>";
		}
	}

	function check_product_category(&$product_category){
		global $problem;

		if(in_array($product_category, array('marvel','dc'))) {
			if( (strcmp($product_category, 'marvel')===0) || (strcmp($product_category, 'dc')===0) ){
				$product_category = filter($product_category);
			}else{
				$problem = TRUE;
				echo "<p id='error'>Invalid categories.</p>";
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>$product_category is not one of the categories.</p>";
		}
	}

	function check_product_size(&$product_size){
		global $problem;

		if(!empty($product_size)){
			if(is_numeric($product_size)){
				$product_size = number_format($product_size, 2);
			}else{
				$problem = TRUE;
				echo "<p id='error'>Invalid height.</p>";
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>Please input something on product size.</p>";
		}
	}

	function check_product_weight(&$product_weight){
		global $problem;

		if(!empty($product_weight)){
			if(is_numeric($product_weight)){
				$product_weight = number_format($product_weight, 2);
			}else{
				$problem = TRUE;
				echo "<p id='error'>$product_weight is not a number.</p>";
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>Please input the product weight.</p>";
		}
	}

	function validate_string(&$product_lam) {
		global $problem;

		if(!empty($product_lam)){
			if( (strlen($product_lam)>=5) && (strlen($product_lam)<=20) ){
				$product_lam = filter($product_lam);
			}else{
				$problem = TRUE;
				echo "<p id='error'>Character length is too short/long.</p>";
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>Please complete this field.</p>";
		}
	}

	function check_selected_option(&$selected_option){
		if(in_array($selected_option, array('yes','no'))){
			if($selected_option == 'yes') {
				$selected_option = 1;
			}else{
				$selected_option = 0;
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>$selected_option is not one of the options.</p>";
		}
	}

	function move_product_image(&$product_image, $image_name, $product_category){
		if(move_uploaded_file($product_image, "images/$product_category/$image_name")){
			return TRUE;
		}
	}

	function INSERT_TO_DB($product_image_name, $product_name, $product_desc, $product_price, $product_quantity, $product_category, $product_size, $product_weight, $product_license, $product_manufacturer, $selected_option){
		require DB;

		$author = $_SESSION['firstname'] . ' ' . $_SESSION['lastname'];

		$query_products = "INSERT INTO `products` (`product_id`, `product_name`, `product_desc`, `product_price`, `product_quantity`, `product_category`, `product_size`, `product_weight`, `product_image`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?)";
		$query_details = "INSERT INTO `details` (`product_id`, `product_license`, `product_manufacturer`, `date_added`, `author`, `featured`, `sold`) VALUES (NULL, ?, ?, NOW(), '$author', ?, 0)";

		if( ($products_statement = $dbc->prepare($query_products)) && ($details_statement = $dbc->prepare($query_details)) ) {
			
			$products_statement->bind_param('ssdisdds', $product_name, $product_desc, $product_price, $product_quantity, $product_category, $product_size, $product_weight, $product_image_name);
			$products_statement->execute();

			$details_statement->bind_param('ssi', $product_license, $product_manufacturer, $selected_option);
			$details_statement->execute();

			if($dbc->affected_rows) {
				echo <<<INSERT_SUCCESSFUL
				<p id='success'>New product successfully added.</p>
				<p> <a class='add_more_product' href='/admin'>Add more products</a> </p>
				<p> <a class='view_products' href='/allproducts'>View products</a> </p>
INSERT_SUCCESSFUL;
			}
		
		}else{
			echo "<p id='error'>Something went wrong while inserting to database.</p>";
		}
	}
?>
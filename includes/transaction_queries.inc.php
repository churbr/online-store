<?php

	// Query to check product ID's existence

	$check_product_id_query = "SELECT `product_id` FROM `products` WHERE `product_id` = ?";

	// Query to fetch particular product in the database

	$view_product_query = "
		SELECT 
		`products`.`product_id`,
		`products`.`product_name`,
		`products`.`product_price`,
		`products`.`product_quantity`,
		`products`.`product_category`,
		`products`.`product_size`,
		`products`.`product_weight`,
		`products`.`product_image`,
		`details`.`product_license`,
		`details`.`product_manufacturer`,
		`details`.`date_added`,
		`details`.`author`

		FROM `products` JOIN `details`
		ON `products`.`product_id` = `details`.`product_id`
		WHERE `products`.`product_id` = ?
	";

	// Query to update products

	$update_products_query = "
		UPDATE `products`
		SET
		`product_name` = ?,
		`product_price` = ?,
		`product_quantity` = ?,
		`product_category` = ?,
		`product_size` = ?,
		`product_weight` = ?
		WHERE `product_id` = ?
	";

	$update_details_query = "
		UPDATE `details`
		SET
		`product_license` = ?,
		`product_manufacturer` = ?,
		`featured` = ?,
		`sold` = ?
		WHERE `product_id` = ?
	";

	// Delete products query

	$delete_product_query = "
		DELETE `products`, `details`
		FROM `products` INNER JOIN `details`
		ON `products`.`product_id` = `details`.`product_id`
		WHERE `products`.`product_id` = ?
	";

	// View all marvel products query

	$fetch_all_marvel_query = "
		SELECT `product_id`, `product_name`, `product_image`
		FROM `products` WHERE `product_category` = 'marvel'
		ORDER BY `product_id` DESC
	";

	// View all dc products query

	$fetch_all_dc_query = "
		SELECT `product_id`, `product_name`, `product_image`
		FROM `products` WHERE `product_category` = 'dc'
		ORDER BY `product_id` DESC
	";

	// By category view

	$view_marvel_category_query = "
		SELECT
		`product_id`,
		`product_name`,
		`product_image`,
		`product_quantity`,
		`product_price`
		FROM `products` WHERE `product_quantity` != 0 AND `product_category` = 'marvel'
		ORDER BY `product_id` DESC
	";

	$view_dc_category_query = "
		SELECT
		`product_id`,
		`product_name`,
		`product_image`,
		`product_quantity`,
		`product_price`
		FROM `products` WHERE `product_quantity` != 0 AND `product_category` = 'dc'
		ORDER BY `product_id` DESC
	";

	/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ TRANSACTION @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

	$check_ccpin_existence_query = "
		SELECT
		`pin_number`
		FROM `accounts`
		WHERE `pin_number` = ?
	";

	$check_product_availability_query = "
		SELECT `product_quantity`
		FROM `products`
		WHERE `product_id` = $product_id
	";
	
	$get_customer_balance_query = "
		SELECT
		`balance`
		FROM `accounts`
		WHERE `pin_number` = ?
	";

	$get_product_paq_query = "
		SELECT
		`product_price`, `product_quantity`
		FROM `products`
		WHERE `product_id` = ?
	";

	$update_product_quantity_query = "
		UPDATE
		`products`
		SET
		`product_quantity` = ?
		WHERE `product_id` = ?
	";

	$update_customer_balance_query = "
		UPDATE
		`accounts`
		SET
		`balance` = ?
		WHERE `pin_number` = ?
	";
?>
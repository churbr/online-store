<?php

	function filter_str($string){
		return strip_tags(trim($string));
	}

	function check_edited_product_name(&$product_name, &$problem) {
		if(!empty($product_name)) {
			if( (strlen($product_name)>=6) && (strlen($product_name)<=100)){
				$product_name = filter_str($product_name);
			}else{
				$problem = TRUE;
				echo "<p id='error'>Fix your product name character length.</p>";
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>Please input your product name.</p>";
		}
	}

	function check_edited_product_price(&$product_price, &$problem) {
		if(is_numeric($product_price)){
			$product_price = floatval($product_price);
		}else{
			$problem = TRUE;
			echo "<p id='error'>Invalid price input.</p>";
		}
	}

	function check_edited_product_quantity(&$product_quantity, &$problem) {
		if(is_numeric($product_quantity)){
			$product_quantity = intval($product_quantity);
		}else{
			$problem = TRUE;
			echo "<p id='error'>Invalid product quantity input.</p>";
		}
	}

	function check_edited_product_category(&$product_category, &$problem) {
		if(in_array($product_category, array('marvel','dc'))) {
			if( (strcmp($product_category, 'marvel')===0) || (strcmp($product_category, 'dc')===0) ){
				$product_category = filter_str($product_category);
			}else{
				$problem = TRUE;
				echo "<p id='error'>Invalid categories.</p>";
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>$product_category is not one of the categories.</p>";
		}
	}

	function check_edited_product_size(&$product_size, &$problem) {
		if(!empty($product_size)){
			if(is_numeric($product_size)){
				$product_size = number_format($product_size, 2);
			}else{
				$problem = TRUE;
				echo "<p id='error'>Invalid height.</p>";
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>Please input something on product size.</p>";
		}
	}

	function check_edited_product_weight(&$product_weight, &$problem){
		if(!empty($product_weight)){
			if(is_numeric($product_weight)){
				$product_weight = number_format($product_weight, 2);
			}else{
				$problem = TRUE;
				echo "<p id='error'>$product_weight is not a number.</p>";
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>Please input the product weight.</p>";
		}
	}

	function check_edited_product_lam(&$product_lam, &$problem) {
		if(!empty($product_lam)){
			if( (strlen($product_lam)>=5) && (strlen($product_lam)<=20) ){
				$product_lam = filter_str($product_lam);
			}else{
				$problem = TRUE;
				echo "<p id='error'>License/Manufacturer character length is too short/long.</p>";
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>Please input license / manufacturer field.</p>";
		}
	}

	function check_edited_select_option(&$selected_option, &$problem){
		if(in_array($selected_option, array('yes','no'))){
			if($selected_option == 'yes') {
				$selected_option = 1;
			}else{
				$selected_option = 0;
			}
		}else{
			$problem = TRUE;
			echo "<p id='error'>$selected_option is not one of the options.</p>";
		}
	}

	function is_loggedin(){
		$loggedin = FALSE;

		if(isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['firstname'], $_SESSION['lastname'], $_SESSION['picture'])){
			$loggedin = TRUE;
		}

		return $loggedin;
	}

	function view_product_data($product_id){

		require 'transaction_queries.inc.php';
		require DB;

		if(is_numeric($product_id)) {

			if($check_product = $dbc->prepare($check_product_id_query)) {
				$check_product->bind_param('i', $product_id);
				$check_product->execute();
			}

			$check_product->store_result();

			if($check_product->num_rows) {
				if($view_selected_product_stmt = $dbc->prepare($view_product_query)) {
					$view_selected_product_stmt->bind_param('i', $product_id);
					$view_selected_product_stmt->execute();

					$view_selected_product_stmt->bind_result($product_id, $product_name, $product_price, $product_quantity, $product_category, $product_size, $product_weight, $product_image, $product_license, $product_manufacturer, $date_added, $author);
					$view_selected_product_stmt->fetch();

					return array(
						'product_id' => $product_id,
						'product_name' => $product_name,
						'product_price' => $product_price,
						'product_quantity' => $product_quantity,
						'product_category' => $product_category,
						'product_size' => $product_size,
						'product_weight' => $product_weight,
						'product_image' => $product_image,
						'product_license' => $product_license,
						'product_manufacturer' => $product_manufacturer,
						'date_added' => $date_added,
						'author' => $author
					);
				}
			}else{ return FALSE; }
		}else{
			return FALSE;
		}
	}

	function check_edited_product($static_product_id, $product_id, $name, $price, $quantity, $category, $size, $weight, $license, $manufacturer, $feature, $sold){
		$problem_message = '';
		$problem = FALSE;

		if($static_product_id===intval($product_id)) {
			check_edited_product_name($name, $problem);
			check_edited_product_price($price, $problem);
			check_edited_product_quantity($quantity, $problem);
			check_edited_product_category($category, $problem);
			check_edited_product_size($size, $problem);
			check_edited_product_weight($weight, $problem);
			check_edited_product_lam($license, $problem);
			check_edited_product_lam($manufacturer, $problem);
			check_edited_select_option($feature, $problem);
			check_edited_select_option($sold, $problem);
		}else{
			$problem = TRUE;
			$problem_message .= "<p id='error'>Product ID is user defined.</p>";
		}

		if(!$problem) {
			return array(
				'id' => $static_product_id,
				'edited_name' => $name,
				'edited_price' => $price,
				'edited_quantity' => $quantity,
				'edited_category' => $category,
				'edited_size' => $size,
				'edited_weight' => $weight,
				'edited_license' => $license,
				'edited_manufacturer' => $manufacturer,
				'edited_feature' => $feature,
				'edited_sold' => $sold
			);
		}
	}

	function UPDATE_PRODUCTS($id, $name, $price, $quantity, $category, $size, $weight, $license, $manufacturer, $feature, $sold) {
		require 'includes/transaction_queries.inc.php';
		require DB;

		if( ($update_products_stmt = $dbc->prepare($update_products_query)) && ($update_details_stmt = $dbc->prepare($update_details_query)) ) {
			
			$update_products_stmt->bind_param('siisddd', $name, $price, $quantity, $category, $size, $weight, $id);
			$update_products_stmt->execute();

			$update_details_stmt->bind_param('ssiii', $license, $manufacturer, $feature, $sold, $id);
			$update_details_stmt->execute();

			if($dbc->affected_rows) {
				$update_products_stmt->free_result();
				$update_details_stmt->free_result();
				$update_products_stmt->close();
				$update_details_stmt->close();
				$dbc->close();

				header("Location: index.php?p=viewproduct&product=$id");
			}else{
				echo "<p id='error'>Product could not be updated.</p>";
			}
		}
	}


	function DELETE_PRODUCT($product_id, &$decision) {
		if(in_array($decision, array('yes', 'no'))) {
			$decision = (strcmp($decision, 'yes')===0) ? 1:0;

			if($decision===1){
				require 'transaction_queries.inc.php';
				require DB;

				if($delete_product_stmt = $dbc->prepare($delete_product_query)) {
					$delete_product_stmt->bind_param('i', $product_id);
					$delete_product_stmt->execute();

					echo "<p id='error'>Product successfully deleted. <br />You will be returned to the main page.</p>";
					header('Refresh:3;url=index.php');
				}
			}else{
				echo "<p id='error'>Delete Cancelled.</p>";
			}
		}
	}

	/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ REPORTS @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

	function display_sales_reports() {
		require DB;

		echo <<<TH
			<thead>
				<th>PRODUCT NAME</th>
				<th>QUANTITY</th>
				<th>AMOUNT</th>
				<th>DATE PURCHASED</th>
			</thead>

		<tbody>
TH;
		$query_sales_reports = $dbc->query("SELECT `products`.`product_name`, `sales`.`product_quantity`, `sales`.`amount`, `sales`.`date_purchased` FROM `sales` JOIN `products` ON `products`.`product_id` = `sales`.`product_id`");

		if(!$query_sales_reports->num_rows) {
			echo "
				<tr>
					<td colspan='4'> <p id='error'>No records yet.</p> </td>
				</tr>
			";
		}

		while($sales_report = $query_sales_reports->fetch_assoc()) {
			$report_product_name = $sales_report['product_name'];
			$report_product_quantity = $sales_report['product_quantity'];
			$report_amount = number_format($sales_report['amount'], 2);
			$report_date_purchased = $sales_report['date_purchased'];

			echo <<<REPORTS
				<tr>
					<td class='product_name'>$report_product_name</td>
					<td class='product_quantity'>$report_product_quantity</td>
					<td class='amount'>Php$report_amount</td>
					<td class='date_purchased'>$report_date_purchased</td>
				</tr>
REPORTS;
		}

	echo '</tbody>';

	}


	function display_top_products() {
		require DB;

		$top_products_query = $dbc->query("
			SELECT
			`products`.`product_name`,
			`details`.`sold`
			FROM
			`products`
			JOIN `details` ON `products`.`product_id` = `details`.`product_id`
			WHERE `details`.`sold` != 0
			ORDER BY `details`.`sold` DESC
		");

		echo '<tbody>';

		if(!$top_products_query->num_rows) {
			echo "
				<tr>
					<td colspan='2'> <p id='error'>No records yet.</p> </td>
				</tr>
			";
		}

		while($top_products = $top_products_query->fetch_assoc()) {
			$top_product_name = $top_products['product_name'];
			$top_product_sold = $top_products['sold'];

			echo <<<TP

				<tr>
					<td class='product_name'>$top_product_name</td>
					<td class='product_quantity'>$top_product_sold</td>
				</tr>
TP;
		}
		echo '</tbody>';
	}


	function display_income() {
		require DB;

		$query_sales_purchased = $dbc->query("SELECT `amount` FROM `sales`");

		while($amount = $query_sales_purchased->fetch_assoc()){
			$income+=$amount['amount'];
		}

		echo '<p id="income">TOTAL INCOME: <span class="price">PHP' . number_format($income, 2) . '</span></p>';
	}

	/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ TRANSACTION @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

	function credit_card_check($ccpin) {
		require 'transaction_queries.inc.php';
		require DB;

		if($check_ccpin_existence_stmt = $dbc->prepare($check_ccpin_existence_query)){
			$check_ccpin_existence_stmt->bind_param('s', $ccpin);
			$check_ccpin_existence_stmt->execute();

			$check_ccpin_existence_stmt->store_result();
			$row = $check_ccpin_existence_stmt->num_rows;

			if($row === 1) {
				return 1;
			}else{
				return 0;
			}
		}

		$check_ccpin_existence_stmt->close();
	}

	function check_product_availability($product_id, $ordered_quantity){
		require 'transaction_queries.inc.php';
		require DB;

		if($check_product_availability_result = $dbc->query($check_product_availability_query)){
			if($check_product_availability_result->num_rows){

				$fetched_quantity = $check_product_availability_result->fetch_assoc();

				$quantity_available = $fetched_quantity['product_quantity'];
				
				if($ordered_quantity<=$quantity_available){
					return TRUE;
				}else{
					return FALSE;
				}
			}
		}

		$check_product_availability_result->close();
	}

	function update_product_quantity($product_id, $new_product_quantity){
		require 'transaction_queries.inc.php';
		require DB;

		if($update_product_quantity_stmt = $dbc->prepare($update_product_quantity_query)){
			$update_product_quantity_stmt->bind_param('ii', $new_product_quantity, $product_id);
			$update_product_quantity_stmt->execute();

			$update_product_quantity_stmt->store_result();
			$updated_pq = $update_product_quantity_stmt->affected_rows;
			$update_product_quantity_stmt->close();

			return $updated_pq;
		}
	}

	function update_customer_balance($customer_ccpin, $remaining_balance){
		require 'transaction_queries.inc.php';
		require DB;

		if($update_customer_balance_stmt = $dbc->prepare($update_customer_balance_query)){
			$update_customer_balance_stmt->bind_param('is', $remaining_balance, $customer_ccpin);
			$update_customer_balance_stmt->execute();

			$update_customer_balance_stmt->store_result();
			$updated_ab = $update_customer_balance_stmt->affected_rows;
			$update_customer_balance_stmt->close();

			return $updated_ab;
		}
	}

	function REPORT($id, $quantity, $amount) {
		require DB;

		if($add_sales_report = $dbc->prepare("INSERT INTO `sales` VALUES(?, ?, ?, NOW())")) {
			$add_sales_report->bind_param('iii', $id, $quantity, $amount);
			$add_sales_report->execute();
		}

		if($update_sold_items = $dbc->prepare("UPDATE `details` SET `sold` = `sold` + ? WHERE `product_id` = ?")) {
			$update_sold_items->bind_param('ii', $quantity, $id);
			$update_sold_items->execute();
		}

		$add_sales_report->close();
		$update_sold_items->close();
	}

	function PURCHASE_PRODUCT($customer_ccpin, $ordered_quantity, $product_id) {
		require 'transaction_queries.inc.php';
		require DB;

		if($get_customer_balance_stmt = $dbc->prepare($get_customer_balance_query)) {
			$get_customer_balance_stmt->bind_param('s', $customer_ccpin);
			$get_customer_balance_stmt->execute();

			$get_customer_balance_stmt->bind_result($fetched_balance);
			$get_customer_balance_stmt->fetch();

			/* Customer's current balance from db */
			$customer_current_balance = $fetched_balance;

			$get_customer_balance_stmt->close();
		}

		if($get_product_paq_stmt = $dbc->prepare($get_product_paq_query)) {
			$get_product_paq_stmt->bind_param('i', $product_id);
			$get_product_paq_stmt->execute();

			$get_product_paq_stmt->bind_result($fetched_product_price, $fetched_current_quantity);
			$get_product_paq_stmt->fetch();

			/* Products current price from db */
			$product_price = $fetched_product_price;

			/* Product's current quantity from db */
			$product_current_quantity = $fetched_current_quantity;

			$get_product_paq_stmt->close();

			/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

			$overall_purchased =  $product_price * $ordered_quantity;

			if(($customer_current_balance-$overall_purchased) >= 0 ){
				// Update quantity
				// Update customer balance

				$new_product_quantity = $product_current_quantity-$ordered_quantity;
				$remaining_balance = $customer_current_balance-$overall_purchased;

				$success_upq = update_product_quantity($product_id, $new_product_quantity);
				$success_ucb = update_customer_balance($customer_ccpin, $remaining_balance);

				REPORT($product_id, $ordered_quantity, $overall_purchased);

				if($success_upq === 1 && $success_ucb===1){
					echo "<p id='success'>Product successfully purchased.</p>";
					header('Refresh: 1');
				}else{
					echo "<p id='error'>There was a problem when processing the transaction.</p>";
				}

			}else{
				echo "<p id='error'>You dont have enough money to buy this product.</p>";
			}
		}
	}

	function PROCESS_TRANSACTION($ccpin, $quantity, $product_id) {

		$submitted_ccpin = strip_tags(trim($ccpin));
		$submitted_quantity = strip_tags(trim($quantity));

		if(is_numeric($submitted_ccpin)){
			if(is_numeric($submitted_quantity)){
				if(credit_card_check($submitted_ccpin)){
					if(check_product_availability($product_id, $quantity)) {
						PURCHASE_PRODUCT($ccpin, $quantity, $product_id);
					}else{
						echo "<p id='error'>Ordered quantity is more than what is available.</p>";
					}
				}else{
					echo "<p id='error'>Creditcard account does not exist.</p>";
				}
			}else{
				echo "<p id='error'>Invalid quantity number.</p>";
			}
		}else{
			echo "<p id='error'>Invalid credit card number.</p>";
		}
	}
?>